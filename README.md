# README #

This README will let you run this project on your local system

### What is this repository for? ###
* Save arithmetic expression and its output in H2 database

* This application is to evaluate a given arithmetic expression e.g input 2+4 should provide an output of 6.0

### How do I get set up? ###

* Checkout this repository using git clone https://vikas_12118@bitbucket.org/vikas_12118/vikas_test.git

* Change directory(cd) to the checkout folder

* There are two ways to run this project
    1. run "mvn clean spring-boot:run" command in the project directory, once you see "Started ArithmeticExpressionApplication" in console, hit http://localhost:8080 in the browser
    2. run "mvn clean package" command in the project directory
        1. copy arithmetic-expression.war from the target directory to webapp folder of your tomcat server
        2. start your tomcat server
        3. hit http://localhost:8080 in the browser
        
        