package com.application.arithmeticexpression.controller;

import com.application.arithmeticexpression.model.Arithmetic;
import com.application.arithmeticexpression.service.ArithmeticExpressionService;
import org.junit.jupiter.api.Test;


import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


@WebMvcTest(controllers = ArithmeticExpressionController.class)
public class ArithmeticExpressionControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ArithmeticExpressionService arithmeticExpressionService;


    @Test
    void testProcessExpression(){
        try {
            Arithmetic arithmetic = new Arithmetic();
            arithmetic.setExpression("2+4");
            Mockito.when(arithmeticExpressionService.evaluateExpression(Mockito.anyString())).thenReturn(6.0);
            MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/processExpression").flashAttr("val", arithmetic))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.model().attribute("val", arithmetic))
                .andReturn();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
