<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@taglib prefix="re" tagdir="/WEB-INF/tags/printResult"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="width=device-width, initial-scale=1" charset=ISO-8859-1">
    <title>Arithmetic Expression Evaluator</title>
    <style>
        input[type=textarea] {
            width: 70%;
            padding: 12px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            resize: vertical;
        }

        /* Style the label to display next to the inputs */
        label {
            padding: 12px 12px 12px 0;
            display: inline-block;
        }

        /* Style the submit button */
        input[type=submit] {
            background-color: #4CAF50;
            color: white;
            padding: 12px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            float: right;
        }

        /* Style the container */
        .container {
            border-radius: 5px;
            background-color: #f2f2f2;
            padding: 20px;
            margin-top: 6px;
        }

        /* Floating column for labels: 25% width */
        .col-25 {
            float: left;
            width: 10%;
            margin-top: 6px;
        }

        /* Floating column for inputs: 75% width */
        .col-75 {
            float: left;
            width: 90%;
            margin-top: 6px;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
            margin-top: 6px;
        }

        .error {
            color: #000;
            background-color: #ffEEEE;
            padding: 8px;
            width: 100%;
            margin-top: 6px;
        }

        /* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
        @media screen and (max-width: 600px) {
            .col-25, .col-75, input[type=submit], input[type=textarea] {
                width: 100%;
                margin-top: 0;
            }
        }
    </style>
</head>
<body>

<h2>Enter Arithmetic Expression To get the result </h2>
<c:if test="${not empty error}">
<div class="error">
    ${error}
</div>
</c:if>
<div class="container">

<form:form method="post" action="processExpression" name="processExpressionForm" modelAttribute="val">
    <div class="row">
        <div class="col-25">
            <form:label path="expression">Enter Expression Here</form:label>
        </div>
        <div class="col-75">
            <form:input path="expression" style="height:100px;width:1500px"/>
        </div>
    </div>
    <div class="row">
        <input type="submit" value="Submit">
    </div>
</form:form>
</div>

<re:printResult inputExpression="${val.exp}" result="${val.output}"/>
</body>
</html>