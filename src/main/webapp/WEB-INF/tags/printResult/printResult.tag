<%@ tag body-content="empty"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<%@ attribute name="inputExpression" rtexprvalue="true" required="true"
              type="java.lang.String" description="Input Expression"%>
<%@ attribute name="result" rtexprvalue="true" required="true"
              type="java.lang.String" description="Display Result"%>

<c:if test="${not empty result}">
    <h4>Expression => <c:out value="${inputExpression}"/>, Result = <c:out value="${result}"/></h4>
</c:if>