package com.application.arithmeticexpression.controller;


import com.application.arithmeticexpression.model.Arithmetic;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ArithmeticExpressionController extends BaseController {

    private static final Logger LOGGER = Logger.getLogger(ArithmeticExpressionController.class);

    /**
     * This method will return start.jsp page
     */
    @GetMapping({"/", "/start"})
    public ModelAndView loadWelcomePage() {
        ModelAndView mav=new ModelAndView("start");
        LOGGER.info("Loading start jsp");
        mav.addObject(VAL, new Arithmetic());
        return mav;
    }

    /**
     * This method takes Arithmetic as a model attribute model and returns the value
     * @param arithmetic {@link Arithmetic}
     * @return {@link ModelAndView}
     */
    @PostMapping(value="/processExpression")
    public ModelAndView processExpression(@ModelAttribute(VAL) Arithmetic arithmetic){
        ModelAndView mav = new ModelAndView();
        mav.setViewName("start");
        mav.addObject(VAL, arithmetic);
        try {
            Double res = arithmeticExpressionService.evaluateExpression(arithmetic.getExpression());
            arithmetic.setOutput(res);
            arithmetic.setExp(arithmetic.getExpression());
            arithmetic.setExpression("");
        }catch (Exception e){
            mav.addObject("error", "Can not evaluate expression, Illegal expression");
        }

        return mav;
    }
}
