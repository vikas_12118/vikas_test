package com.application.arithmeticexpression.controller;

import com.application.arithmeticexpression.service.ArithmeticExpressionService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseController {

    protected static final String VAL = "val";

    @Autowired
    ArithmeticExpressionService arithmeticExpressionService;
}
