package com.application.arithmeticexpression.repositories;

import com.application.arithmeticexpression.entity.ArithmeticEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArithmeticRepository extends JpaRepository<ArithmeticEntity, Integer> {
}
