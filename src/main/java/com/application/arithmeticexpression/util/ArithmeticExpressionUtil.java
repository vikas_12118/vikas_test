package com.application.arithmeticexpression.util;
import java.util.function.DoubleSupplier;
import java.util.function.Predicate;

public class ArithmeticExpressionUtil {

    public static double output(final String str) {

        return new Object() {
            int count = -1, currentChar;
            Predicate<Character> getAndMove = characterPredicate -> {
                while (currentChar == ' ') getNext();
                if (currentChar == characterPredicate) {
                    getNext();
                    return true;
                }
                return false;
            };

            DoubleSupplier func = () -> {
                double x = getValue();
                while (true) {
                    if (getAndMove.test('*'))
                        x *= getValue();
                    else if (getAndMove.test('/'))
                        x /= getValue();
                    else return x;
                }
            };


            DoubleSupplier evaluateExp = () -> {
                double x = func.getAsDouble();
                while (true) {
                    if (getAndMove.test('+'))
                        x += func.getAsDouble();
                    else if (getAndMove.test('-'))
                        x -= func.getAsDouble();
                    else return x;
                }
            };

            void getNext() {
                currentChar = (++count < str.length()) ? str.charAt(count) : -1;
            }

            double processExpression() {
                getNext();
                double x = evaluateExp.getAsDouble();
                if (count < str.length()) throw new RuntimeException("Illegal: " + (char)currentChar);
                return x;
            }


            double getValue() {
                if (getAndMove.test('+'))
                    return getValue();
                if (getAndMove.test('-'))
                    return -getValue();

                double x;
                int startPos = this.count;
                if (getAndMove.test('(')) {
                    x = evaluateExp.getAsDouble();
                    getAndMove.test(')');
                }
                else if ((currentChar >= '0' && currentChar <= '9') || currentChar == '.') {
                    while ((currentChar >= '0' && currentChar <= '9') || currentChar == '.')
                        getNext();
                    x = Double.parseDouble(str.substring(startPos, this.count));
                }

                else {
                    throw new RuntimeException("Illegal: " + (char)currentChar);
                }


                return x;
            }

        }.processExpression();
    }
}
