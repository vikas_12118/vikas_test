package com.application.arithmeticexpression.service;

import com.application.arithmeticexpression.entity.ArithmeticEntity;
import com.application.arithmeticexpression.repositories.ArithmeticRepository;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseService {

    @Autowired
    protected ArithmeticRepository arithmeticRepository;

    /**
     * Saved in H2 database
     * @param expression
     * @param output
     */
    protected void saveArithmetic(String expression, Double output) {
        ArithmeticEntity entity = new ArithmeticEntity();
        entity.setExpression(expression);
        entity.setOutput(output);
        arithmeticRepository.save(entity);
    }
}
