package com.application.arithmeticexpression.service.impl;

import com.application.arithmeticexpression.service.ArithmeticExpressionService;
import com.application.arithmeticexpression.service.BaseService;
import com.application.arithmeticexpression.util.ArithmeticExpressionUtil;
import org.springframework.stereotype.Service;

@Service
public class ArithmeticExpressionServiceImpl extends BaseService implements ArithmeticExpressionService {
    @Override
    public Double evaluateExpression(String expression) {
        Double output = ArithmeticExpressionUtil.output(expression);
        saveArithmetic(expression, output);
        return output;
    }
}
