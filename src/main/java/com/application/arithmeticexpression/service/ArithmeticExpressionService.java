package com.application.arithmeticexpression.service;


public interface ArithmeticExpressionService {
    public Double evaluateExpression(String expression);
}
